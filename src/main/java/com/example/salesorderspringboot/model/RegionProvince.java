package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "region_province")
@Setter
@Getter
public class RegionProvince implements Serializable {

    @Id
    @SequenceGenerator(
            name = "rpModelSequence",
            sequenceName = "reqion_province_seq",
            allocationSize = 1,
            initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rpModelSequence")
    @Column(name = "id")
    public int id;

    @Column(name = "province_code")
    public String provinceCode;

    @Column(name = "province_name")
    public String provinceName;
}
