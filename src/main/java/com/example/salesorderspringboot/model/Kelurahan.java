package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "kelurahan")
@Setter
@Getter
public class Kelurahan implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "village_code")
    public String villageCode;

    @Column(name = "postal_code")
    public String postalCode;

    @Column(name ="sub_district_code")
    public String subDistrictCode;

    @Column(name ="village_name")
    public String villageName;

    @Column(name = "id")
    public Long id;

    @Column(name ="city_code")
    public String cityCode;

    @Column(name ="kode_pos")
    public String kodePos;

    @Column(name ="kecamatan")
    public String kecamatan;

    @Column(name ="kelurahan")
    public String kelurahan;

}
