package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "kecamatan")
@Setter
@Getter
public class Kecamatan implements Serializable {

    @Id
    @Column(name = "sub_district_code")
    public String subDistrictCode;

    @Column(name = "city_code")
    public String cityCode;

    @Column(name = "sub_district_name")
    public String subDistrictName;

    @Column(name = "id")
    public Long id;

    @Column(name = "kecamatan")
    public String kecamatan;
}
