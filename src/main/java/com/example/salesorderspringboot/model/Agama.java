package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "agama")
@Setter
@Getter
public class Agama implements Serializable{

    @Id
    @Column(name = "code")
    public String code;

    @Column(name = "description")
    public String description;
}
