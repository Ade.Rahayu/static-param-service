package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "provinsi")
@Setter
@Getter
public class Provinsi implements Serializable {
    @Id
    @Column(name = "province_code")
    public String provinceCode;

    @Column(name = "province_name")
    public String provinceName;
}
