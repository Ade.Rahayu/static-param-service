package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "kota_kabupaten")
@Setter
@Getter
public class KotaKab implements Serializable {

    @Id
    @Column(name = "city_code")
    public String cityCode;

    @Column(name = "city_name")
    public String cityName;

    @Column(name = "province_code")
    public String provinceCode;
}
