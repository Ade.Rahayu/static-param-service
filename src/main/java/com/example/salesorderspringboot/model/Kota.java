package com.example.salesorderspringboot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "kota")
@Setter
@Getter
public class Kota implements Serializable {

    @Id
    @Column(name = "city_code")
    public String cityCode;

    @Column(name = "city")
    public String city;

    @Column(name = "state_code")
    public String stateCode;
}
