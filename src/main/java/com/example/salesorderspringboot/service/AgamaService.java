package com.example.salesorderspringboot.service;

import com.example.salesorderspringboot.model.Agama;
import com.example.salesorderspringboot.model.Kecamatan;
import com.example.salesorderspringboot.repository.AgamaRepository;
import com.example.salesorderspringboot.util.GeneralConstants;
import com.example.salesorderspringboot.util.IAction;
import com.example.salesorderspringboot.util.SimpleResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class AgamaService implements IAction {
    @Autowired
    AgamaRepository agamaRepository;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null || customId.get("userId").toString().equalsIgnoreCase(""))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, "");

            Agama agama= om.convertValue(param, Agama.class);

            //proses validasi request
            if (agama.code == null || GeneralConstants.EMPTY_STRING.equals(agama.code))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "code can't be null or empty", new String());

            if (agama.description == null || GeneralConstants.EMPTY_STRING.equals(agama.description))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "description can't be null or empty", new String());


            Optional<Agama> byCode = agamaRepository.findByCode(agama.code);

            if (byCode.isPresent())
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "code already exists", getAgama(byCode.get().code, true));

            agamaRepository.save(agama);

            SimpleResponse simpleResponse = new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getAgama(String.valueOf(agama.code), true));
            return simpleResponse;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse getAll(Pageable pageable) {
        return null;
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        return null;
    }

    @Override
    public SimpleResponse entity(Object param) {
        return null;
    }

    public <byCode> Map<String, Object> getAgama(String code, boolean b) {
        Optional<Agama> byCode = agamaRepository.findByCode(code);
        Map<String, Object> result = new HashMap<>();
        if (byCode.isPresent()) {
            result.put("code", byCode.get().code);
            result.put("description", byCode.get().description);
        }
        return result;
    }
}
