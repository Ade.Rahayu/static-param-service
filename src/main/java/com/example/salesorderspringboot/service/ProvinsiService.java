package com.example.salesorderspringboot.service;

import com.example.salesorderspringboot.model.Kecamatan;
import com.example.salesorderspringboot.model.Provinsi;
import com.example.salesorderspringboot.repository.ProvinsiRepository;
import com.example.salesorderspringboot.util.GeneralConstants;
import com.example.salesorderspringboot.util.IAction;
import com.example.salesorderspringboot.util.SimpleResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class ProvinsiService implements IAction {
    @Autowired
    ProvinsiRepository provinsiRepository;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null || customId.get("userId").toString().equalsIgnoreCase(""))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, "");

            Provinsi provinsi = om.convertValue(param, Provinsi.class);

            //proses validasi request
            if (provinsi.provinceCode == null || GeneralConstants.EMPTY_STRING.equals(provinsi.provinceCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "province code can't be null or empty", new String());

            if (provinsi.provinceName == null || GeneralConstants.EMPTY_STRING.equals(provinsi.provinceName))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "province name can't be null or empty", new String());


            Optional<Provinsi> byProvinceCode = provinsiRepository.findByProvinceCode(provinsi.provinceCode);

            if (byProvinceCode.isPresent())
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "province code already exists", getProvinsi(byProvinceCode.get().provinceCode, true));

            provinsiRepository.save(provinsi);

            SimpleResponse simpleResponse = new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProvinsi(String.valueOf(provinsi.provinceCode), true));
            return simpleResponse;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse getAll(Pageable pageable) {
        try {
            Page<Provinsi> allProvinsi= provinsiRepository.findAll(pageable);
            HashMap<Object, Object> map = new HashMap<>();
            map.put("data", allProvinsi);
            map.put("page", allProvinsi.getPageable().getPageNumber());
            map.put("size",allProvinsi.getPageable().getPageSize());
            map.put("total", allProvinsi.getTotalElements());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, map);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, Map.class);

            if(req.get("provinceCode") == null){
                return null;
            }
            String provinceCode = req.get("provinceCode").toString();
            List<Provinsi> provinsiList = provinsiRepository.findByProvinceCodeIsContaining(provinceCode);

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS,provinsiList);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        return null;
    }

    public <byProvinceCode> Map<String, Object> getProvinsi(String provinceCode, boolean b) {
        Optional<Provinsi> byProvinceCode = provinsiRepository.findByProvinceCode(provinceCode);
        Map<String, Object> result = new HashMap<>();
        if (byProvinceCode.isPresent()) {
            result.put("provinceCode", byProvinceCode.get().provinceCode);
            result.put("provinceName", byProvinceCode.get().provinceName);
        }
        return result;
    }
}
