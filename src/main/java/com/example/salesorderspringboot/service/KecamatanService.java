package com.example.salesorderspringboot.service;

import com.example.salesorderspringboot.model.Kecamatan;
import com.example.salesorderspringboot.repository.KecamatanRepository;
import com.example.salesorderspringboot.util.GeneralConstants;
import com.example.salesorderspringboot.util.IAction;
import com.example.salesorderspringboot.util.SimpleResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class KecamatanService implements IAction {
    @Autowired
    KecamatanRepository kecamatanRepository;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null || customId.get("userId").toString().equalsIgnoreCase(""))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, "");

            Kecamatan kecamatan = om.convertValue(param, Kecamatan.class);

            //proses validasi request
            if (kecamatan.subDistrictCode == null || GeneralConstants.EMPTY_STRING.equals(kecamatan.subDistrictCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sub district code can't be null or empty", new String());

            if (kecamatan.cityCode == null || GeneralConstants.EMPTY_STRING.equals(kecamatan.cityCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "city code can't be null or empty", new String());

            if (kecamatan.subDistrictName == null || GeneralConstants.EMPTY_STRING.equals(kecamatan.subDistrictName))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sub district name can't be null or empty", new String());

            if (kecamatan.id == null || GeneralConstants.EMPTY_STRING.equals(kecamatan.id))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id can't be null or empty", new String());

            if (kecamatan.kecamatan == null || GeneralConstants.EMPTY_STRING.equals(kecamatan.kecamatan))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "kecamatan can't be null or empty", new String());


            Optional<Kecamatan> bySDCode = kecamatanRepository.findBySubDistrictCode(kecamatan.subDistrictCode);

            if (bySDCode.isPresent())
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sub district code already exists", getKecamatan(bySDCode.get().subDistrictCode, true));

            kecamatanRepository.save(kecamatan);

            SimpleResponse simpleResponse = new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getKecamatan(String.valueOf(kecamatan.subDistrictCode), true));
            return simpleResponse;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse getAll(Pageable pageable) {
        try {
            Page<Kecamatan> allKecamatan = kecamatanRepository.findAll(pageable);
            HashMap<Object, Object> map = new HashMap<>();
            map.put("data", allKecamatan);
            map.put("page", allKecamatan.getPageable().getPageNumber());
            map.put("size",allKecamatan.getPageable().getPageSize());
            map.put("total", allKecamatan.getTotalElements());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, map);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, Map.class);

            if(req.get("subDistrictName") == null){
                return null;
            }
            String subDistrictName = req.get("subDistrictName").toString();
            List<Kecamatan> kecamatanList = kecamatanRepository.findBySubDistrictNameIsContaining(subDistrictName);

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, kecamatanList);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        return null;
    }

    public <bySDCode> Map<String, Object> getKecamatan(String subDistrictCode, boolean b) {
        Optional<Kecamatan> bySDCode = kecamatanRepository.findBySubDistrictCode(subDistrictCode);
        Map<String, Object> result = new HashMap<>();
        if (bySDCode.isPresent()) {
            result.put("subDistrictCode", bySDCode.get().subDistrictCode);
            result.put("cityCode", bySDCode.get().cityCode);
            result.put("subDistrictName", bySDCode.get().subDistrictName);
            result.put("id", bySDCode.get().id);
            result.put("kecamatan", bySDCode.get().kecamatan);

        }
        return result;
    }
}
