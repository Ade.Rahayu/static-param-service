package com.example.salesorderspringboot.service;

import com.example.salesorderspringboot.model.Kelurahan;

import com.example.salesorderspringboot.repository.KelurahanRepository;
import com.example.salesorderspringboot.util.GeneralConstants;
import com.example.salesorderspringboot.util.IAction;
import com.example.salesorderspringboot.util.SimpleResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;


import java.util.*;


@Service
@Slf4j
public class KelurahanService implements IAction {
    @Autowired
    KelurahanRepository kelurahanRepository;

    @Override
    public SimpleResponse insert(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null || customId.get("userId").toString().equalsIgnoreCase(""))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, "");

            Kelurahan kelurahan = om.convertValue(param, Kelurahan.class);

            //proses validasi request
            if (kelurahan.villageCode == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.villageCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "village code can't be null or empty", new String());

            if (kelurahan.subDistrictCode == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.subDistrictCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sub district code can't be null or empty", new String());

            if (kelurahan.cityCode == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.cityCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "city code can't be null or empty", new String());

            if (kelurahan.id == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.id))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id can't be null or empty", new String());

            if (kelurahan.kelurahan == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.kelurahan))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "kelurahan can't be null or empty", new String());

            if (kelurahan.postalCode == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.postalCode))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "postal code can't be null or empty", new String());

            if (kelurahan.kodePos == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.kodePos))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "kode pos can't be null or empty", new String());

            if (kelurahan.villageName == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.villageName))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "village name can't be null or empty", new String());

            if (kelurahan.kecamatan == null || GeneralConstants.EMPTY_STRING.equals(kelurahan.kecamatan))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "kecamatan can't be null or empty", new String());


            Optional<Kelurahan> byVillageCode = kelurahanRepository.findByVillageCode(kelurahan.villageCode);

            if (byVillageCode.isPresent())
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "village code already exists", getKelurahan(byVillageCode.get().villageCode, true));

            kelurahanRepository.save(kelurahan);

            SimpleResponse simpleResponse = new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getKelurahan(String.valueOf(kelurahan.villageCode), true));
            return simpleResponse;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    @GetMapping
    public SimpleResponse getAll(Pageable pageable) {
        try {
            Page<Kelurahan> allKelurahan = kelurahanRepository.findAll(pageable);
            HashMap<Object, Object> map = new HashMap<>();
            map.put("data", allKelurahan);
            map.put("page", allKelurahan.getPageable().getPageNumber());
            map.put("size",allKelurahan.getPageable().getPageSize());
            map.put("total", allKelurahan.getTotalElements());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, map);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, Map.class);

            if(req.get("villageName") == null){
                return null;
            }
            String villageName = req.get("villageName").toString();
            List<Kelurahan> kelurahanList = kelurahanRepository.findByVillageNameIsContaining(villageName);

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, kelurahanList);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        return null;
    }

    public <byVCode> Map<String, Object> getKelurahan(String villageCode, boolean b) {
        Optional<Kelurahan> byVillageCode = kelurahanRepository.findByVillageCode(villageCode);
        Map<String,Object> result = new HashMap<>();
        if (byVillageCode.isPresent()) {
            result.put("villageCode", byVillageCode.get().villageCode);
            result.put("cityCode", byVillageCode.get().cityCode);
            result.put("villageName", byVillageCode.get().villageName);
            result.put("id", byVillageCode.get().id);
            result.put("postalCode", byVillageCode.get().postalCode);
            result.put("subDistrictCode", byVillageCode.get().subDistrictCode);
            result.put("kecamatan", byVillageCode.get().kecamatan);
            result.put("kelurahan", byVillageCode.get().kelurahan);
            result.put("kodePos", byVillageCode.get().kodePos);

        }
        return result;
        
    }

}
