package com.example.salesorderspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesOrderSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesOrderSpringbootApplication.class, args);

		System.out.println("------------RUNNING APLIKASI------------");
	}

}
