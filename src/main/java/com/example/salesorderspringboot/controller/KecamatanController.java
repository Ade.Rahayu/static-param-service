package com.example.salesorderspringboot.controller;

import com.example.salesorderspringboot.service.KecamatanService;
import com.example.salesorderspringboot.util.GeneralConstants;
import com.example.salesorderspringboot.util.IAction;
import com.example.salesorderspringboot.util.SimpleResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/static/kecamatan")
public class KecamatanController implements IAction {
    @Autowired
    KecamatanService service;

    @PostMapping(value = "/insert", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public SimpleResponse insert(@RequestBody Object param, @RequestHeader("X-Consumer-Custom-ID") String header) {
        return service.insert(param, header);
    }

    @GetMapping(value = "/getAll")
    @Override
    public SimpleResponse getAll(Pageable pageable) {

        try {
            return service.getAll(pageable);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @PostMapping(value = "/inquiry", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public SimpleResponse inquiry(@RequestBody Object param) {
        return service.inquiry(param);
    }

    @Override
    public SimpleResponse entity(Object param) {
        return null;
    }
}
