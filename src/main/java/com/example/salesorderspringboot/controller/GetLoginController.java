package com.example.salesorderspringboot.controller;

import com.example.salesorderspringboot.util.GeneralConstants;
import com.example.salesorderspringboot.util.SimpleResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/api/static")
public class GetLoginController {

    @GetMapping("/getLogin")
    public SimpleResponse getLogin() {
        RestTemplate restTemplate = new RestTemplate();
        HashMap<Object, Object> map = new HashMap<>();
        map.put("username", "adeintanr");
        map.put("password", "@Adeade1");
        HttpEntity<Object> request = new HttpEntity<>(map);
        //HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String url = "http://localhost:8080/api/v1/login";
        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
        log.info("response: " + response);
        return new SimpleResponse(1L, "", response);
    }
}
