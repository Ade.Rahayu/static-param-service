package com.example.salesorderspringboot.repository;

import com.example.salesorderspringboot.model.Kecamatan;
import com.example.salesorderspringboot.model.Provinsi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProvinsiRepository extends JpaRepository<Provinsi, Long> {
    Optional<Provinsi> findByProvinceCode(@Param("provinceCode") String provinceCode);

    List<Provinsi> findByProvinceCodeIsContaining(String provinceCode);

    Page<Provinsi> findAll(Pageable pageable);
}
