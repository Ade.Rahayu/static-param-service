package com.example.salesorderspringboot.repository;

import com.example.salesorderspringboot.model.Kecamatan;
import com.example.salesorderspringboot.model.Kelurahan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KecamatanRepository extends JpaRepository<Kecamatan, Long> {
    Optional<Kecamatan> findBySubDistrictCode(@Param("subDistrictCode") String subDistrictCode);

    List<Kecamatan> findBySubDistrictNameIsContaining(String subDistrictName);

    Page<Kecamatan> findAll(Pageable pageable);
}
