package com.example.salesorderspringboot.repository;

import com.example.salesorderspringboot.model.Agama;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long> {
    Optional<Agama> findByCode(@Param("code") String code);

//    List<Agama> findByNameIsContaining(String description);
//
//    Page<Agama> findAll(Pageable pageable);
}
