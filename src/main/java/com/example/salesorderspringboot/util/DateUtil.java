package com.example.salesorderspringboot.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateUtil {
    public static final String ISO_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     * LocalDateTime --> String
     * Convert datetime to desire format, timezone and localization
     * eg. DateUtil.convertLocalDateTimeToString(localDateTime, "dd MMMM YYYY HH:mm:ss", ZoneId.of("Asia/Jakarta"), new Locale("id"));
     **/
    public static String convertLocalDateTimeToString(LocalDateTime datetime, String format, ZoneId zoneId, Locale locale) {
        OffsetDateTime zoneDateTime = OffsetDateTime.from(datetime.atZone(ZoneId.systemDefault()).toInstant().atZone(zoneId));
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format).localizedBy(locale);
        return zoneDateTime.format(dtf);
    }

    public static String convertLocalDateTimeToString(LocalDateTime datetime, String format) {
        return convertLocalDateTimeToString(datetime, format, ZoneId.systemDefault(), new Locale("id"));
    }

    /**
     * Default LocalDateTime to String converter with ISO_PATTERN format
     **/
    public static String convertLocalDateTimeToString(LocalDateTime dateFrom) {
        return convertLocalDateTimeToString(dateFrom, ISO_PATTERN);
    }

    /**
     * String --> LocalDateTime
     * Convert string to datetime with selected zone
     **/
    public static LocalDateTime convertFromStringToLocalDateTime(String dateFrom, String format, ZoneId zoneId) throws ParseException {
        SimpleDateFormat genericFormat = new SimpleDateFormat(format);
        return genericFormat.parse(dateFrom).toInstant().atZone(zoneId).toLocalDateTime();
    }

    public static LocalDateTime convertFromStringToLocalDateTime(String dateFrom) throws ParseException {
        return convertFromStringToLocalDateTime(dateFrom, ISO_PATTERN, ZoneId.systemDefault());
    }

    public static LocalDateTime now() {
        return LocalDateTime.now(ZoneId.systemDefault());
    }

}
