package com.example.salesorderspringboot.util;

public class SimpleResponse {private Long status;
    private String message;
    private Object content;

    public SimpleResponse(Long status, String message, Object content) {
        super();
        this.status = status;
        this.message = message;
        this.content = content;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

}
