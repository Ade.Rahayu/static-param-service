package com.example.salesorderspringboot.util;


import org.springframework.data.domain.Pageable;

public interface IAction {
    SimpleResponse insert(Object param, String header);
    SimpleResponse getAll(Pageable pageable);
    SimpleResponse inquiry(Object param);
    SimpleResponse entity(Object param);
}
